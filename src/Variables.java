public class Variables {
    public static void main(String[] args) {
        int myInt = 1;
        System.out.println(myInt);
        short myShort = 3266;
        long myLong = 256435466;
        System.out.println(myShort);
        System.out.println(myLong);
        double myDouble = 235.35;
        float myFloat = 453.56f;
        System.out.println(myDouble);
        char c = 'a';
        boolean b = true;
        System.out.println(c);
        byte myByte = 100;//-128-127
        System.out.println(myByte);
    }
}
