import java.util.Scanner;

public class DoWhile {
    public static void main(String[] args) {
        Scanner sc_int = new Scanner(System.in);
        int value;
        do {
            System.out.println("Input 5:");
            value = sc_int.nextInt();
        } while (value != 5);
        System.out.println("5 is input");
    }
}
