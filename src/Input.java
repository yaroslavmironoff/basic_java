import java.util.Scanner;

public class Input {
    public static void main(String[] args){
        String s = "string";
        String s_new = new String("new string");
        Scanner sc = new Scanner(System.in);
        System.out.println("input something");
        String str = sc.nextLine();
        System.out.println("input is: " + str);
        System.out.println("input some int");
        Scanner scan_int = new Scanner(System.in);
        int i = scan_int.nextInt();
        System.out.println("Some int: " + i);
    }
}
