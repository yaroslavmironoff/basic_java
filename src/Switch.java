import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input age");
        String x = scanner.nextLine();
        switch (x) {
            case "zero":
                System.out.println("birth");
                break;
            case "eight":
                System.out.println("school");
                break;
            case "eighteen":
                System.out.println("graduated from school");
                break;
        }

    }
}
