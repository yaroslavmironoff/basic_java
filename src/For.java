public class For {
    public static void main(String[] args) {
        System.out.println("Numbers from 0 to 10");
        for (int i = 0; i <= 10; i++) {
            System.out.println("Number = " + i);//sout 11 numbers
        }
        System.out.println("Numbers from 10 to 0");
        for (int i = 10; i >= 0; i--) {
            System.out.println("Number = " + i);//sout 11 numbers
        }
    }
}
