public class If {
    public static void main(String[] args) {
        int myInt = 5;
        if (myInt < 10) {
            System.out.println("if");
        }
        else if(myInt > 20){
            System.out.println("else if");
        }
        else {
            System.out.println("else");
        }
    }
}
